package model;

import javax.swing.JLabel;

public class Step
{
	private String name;
	private String toolName;
	private String description;
	private String instruction;
	private JLabel jLabel;
	private String pathExecTool;
	private String pathFile;
	private int idSoft;
	
	public Step(String name, String toolName, String description, String instruction, JLabel jLabel, String pathExecTool, String pathLString, int idSoft)
	{
		this.setName(name);
		this.setToolName(toolName);
		this.setDescription(description);
		this.setInstruction(instruction);
		this.setjLabel(jLabel);
		this.setPathExecTool(pathExecTool);
		this.setPathFile(pathLString);
		this.setIdSoft(idSoft);
	}
	
	//Getter / Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToolName() {
		return toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}


	public JLabel getjLabel() {
		return jLabel;
	}


	public void setjLabel(JLabel jLabel) {
		this.jLabel = jLabel;
	}


	public String getPathExecTool() {
		return pathExecTool;
	}


	public void setPathExecTool(String pathExecTool) {
		this.pathExecTool = pathExecTool;
	}


	public String getPathFile() {
		return pathFile;
	}


	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	public int getIdSoft() {
		return idSoft;
	}

	public void setIdSoft(int idSoft) {
		this.idSoft = idSoft;
	}
	
	
	
}
