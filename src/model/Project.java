package model;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JLabel;

import org.json.simple.JSONObject;

import tool.FileIO;
import tool.Json;
import tool.SVN;

import aglinterface.Window;
import parsers.*;

public final class Project
{
	private String name;
	private String path;
	private String remoterepositorypath;
	private static Project INSTANCE = null;

	private String wordPath = "";
	private String wordFile = "";
	
	private int index = 0;
	
	private ArrayList<Step> stepList = new ArrayList<Step>();
	
	private Project()
	{}
	
	public static Project getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Project();
		}
		return INSTANCE;
	}
	
	//Init all data
	//Copy/Paste default files from sourceDoc to user directory
	public void setStepListInfo()
	{
		String name = "";
		String toolName = "";
		String description = "";
		String instruction = "";
		JLabel jLabel = null;
		String pathExecTool = "";
		String pathFile = "";
		int idSoft = 0;
	
		Project project = Project.getInstance();
		JSONObject jsonObject = Json.loadFromFile("src\\config\\execpath.json");
		
		name = "Conception 1/2";
		toolName = "Dia";
		description = "Durant la phase de conception de l�AGL, le Concepteur doit avoir pris connaissance des sp�cifications afin de concevoir le diagramme de classe correspondant."; 
		instruction = "Utilisation de " + toolName +":\nUne fois le logiciel Dia lanc�, cr�ez votre diagramme de classe directement dans l'outil via la barre d'outils\nUne fois votre mod�lisation termin�e, enregistrer votre travail puis fermer l'outil\nCliquez sur suivant pour passer � la deuxi�me �tape de la conception";
		jLabel = Window.lblleftmenuconception;
		pathExecTool = (String) jsonObject.get(toolName);
		pathFile = project.getPath() + "\\" + project.getName() + ".dia";
		FileIO.copyPasteFile("src\\sourceDoc\\dia.dia", pathFile);
		idSoft = 0;
		
		Project.getInstance().getStepList().add(new Step(name, toolName, description, instruction, jLabel, pathExecTool, pathFile, idSoft));

		//DiaToCode
		name = "Conception 2/2";
		toolName = "Dia To Code";
		description = "Dia To Code va permettre de concr�tiser le travail effectuer avec Dia lors de la phase pr�c�dente";
		instruction = "Utilisation de " + toolName + " :\nCette deuxi�me �tape de conception va permettre de g�n�rer les fichiers java correspondant � votre conception.\nCliquez sur suivant pour passer � la phase de r�alisation avec NetBeans";
		jLabel = Window.lblleftmenuconception2;
		pathExecTool = (String) jsonObject.get(toolName);
		pathFile = project.getPath() + "\\" + project.getName() + ".dia";
		//FileIO.copyPasteFile("src\\sourceDoc\\dia.dia", pathFile);
		idSoft = 1;
		
		Project.getInstance().getStepList().add(new Step(name, toolName, description, instruction, jLabel, pathExecTool, pathFile, idSoft));

		name = "R�alisation";
		toolName = "NetBeans";
		description = "Cette �tape vous permet de sp�cifier le fonctionnement de votre outil";
		instruction = "Instruction " + toolName + " :\nUne fois NetBeans ouvert, cr�er votre interface graphique et le code manquant pour votre futur outil.";
		jLabel = Window.lblleftmenurealisation;
		pathExecTool = (String) jsonObject.get(toolName);
		pathFile = project.getPath() + "\\IntgAGL";
		idSoft = 2;
		
		Project.getInstance().getStepList().add(new Step(name, toolName, description, instruction, jLabel, pathExecTool, pathFile, idSoft));

		name = "Test";
		toolName = "JUnit";
		description = "La phase de test est l�occasion de tester la robustesse et la conformit� du logiciel produit avec les exigences clients. Les testeurs au travers de l�AGL acc�dent aux fichiers Java dans lesquels ils codent, gr�ce notamment aux annotations, les tests de chacune des m�thodes.";
		instruction = "Instruction " + toolName + " :\nAcc�dez directement � la cr�ation de vos test via NetBeans\nSur une classe faire Ctrl+Maj+U puis �crire ses tests.";
		jLabel = Window.lblleftmenutest;
		pathExecTool = null;
		pathFile = null;
		idSoft = 0;
		
		Project.getInstance().getStepList().add(new Step(name, toolName, description, instruction, jLabel, pathExecTool, pathFile, idSoft));
		
		name = "Documentation";
		toolName = "Doxygene";
		description = "description de la phase " + name + "avec l'outil " + toolName;
		instruction = "Instruction " + toolName + " :\nAcc�dez directement � la cr�ation de votre documentation via NetBeans\nCliquer : Menu Run > Generate Doxygen";
		jLabel = Window.lblDocumentation;
		pathExecTool = null;
		pathFile = null;
		idSoft = 0;
		
		Project.getInstance().getStepList().add(new Step(name, toolName, description, instruction, jLabel, pathExecTool, pathFile, idSoft));

	/*
		name = "Configuration";
		toolName = "Cucumber";
		description = "description de la phase " + name + "avec l'outil " + toolName;
		instruction = "instruction utilisation " + toolName;
		jLabel = Window.lblleftmenuconfiguration;
		pathExecTool = null;
		pathFile = null;
		idSoft = 0;
		
		Project.getInstance().getStepList().add(new Step(name, toolName, description, instruction, jLabel, pathExecTool, pathFile, idSoft));
	*/
		//Init Word Path 
		Project.getInstance().setWordPath((String) jsonObject.get("Word"));
		Project.getInstance().setWordFile(project.getPath() + "\\" + project.getName() + ".docx");
		FileIO.copyPasteFile("src\\sourceDoc\\word.docx", project.getWordFile());
		//**FileIO.copyPasteFile("sourceDoc\\word.docx", project.getWordFile());

		//Init Netbeans Folder
		FileIO.copyPasteFolder("src\\sourceDoc\\Netbean", project.getPath());
		//**FileIO.copyPasteFolder("sourceDoc\\Netbean", project.getPath());
		
		//Init GUI with list data
		initGUI();		
	}
	
	//Generate files and folders
	public static void createEnvironnement()
	{
		Project project = Project.getInstance();
		try
		{
			//Create the main directory
			new File(project.getPath()).mkdir();
			//Create OSRM directory into main directory
			new File(project.getPath() + "\\OSRMT").mkdir();
			
			// Pour le SVN
			SVN svn = new SVN();
			//svn.command("add *", project.getRemoterepositorypath());
			svn.command("add " + project.getName() + "\\*", project.getRemoterepositorypath());
			svn.command("commit -m 'update'", project.getRemoterepositorypath());
		}
		catch(Exception a)
		{
			System.out.println(a.toString());
		}
	}
	
	
	public static void launchWord()
	{
		Project project = Project.getInstance();
		String path = project.getWordPath();
		String file = project.getWordFile();
		
		launchApplication(path, file);
	}
	
	public static void launchDiaCode(String diaCodePath, String cheminDia, String folderDest)
	{
		try
		{
			String res = "";
			Runtime run = Runtime.getRuntime();
			String[] cmd =  {diaCodePath, "-t", "java", cheminDia, "-d", folderDest};
			for (String s : cmd)
			{
				res += s+ " ";
			}
			System.out.println(res);
			run.exec(cmd);
			
			Thread.sleep(3000);
			
			ImplementationParser.parseFolder(Project.getInstance().getPath() + "\\IntgAGL\\src\\intgagl\\");
			javax.swing.JOptionPane.showMessageDialog(null,"Le code a ete g�n�r� avec succ�s");
			
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//Launch NetBeans
	public static void launchNetBeans(String cheminNetbeans, String cheminProjet)
	{
	// [Chemin vers exec netbeans] --open [Chemin vers le dossier du projet (le dossier qui contient le dossier nbproject)]	
		
		/*
		File file = new File(Project.getInstance().getPath() + "\\JAVA");
		File[] files = file.listFiles();
		System.out.println("File path : " + Project.getInstance().getPath() + "\\JAVA" + " File Size : " + files.length);
		 
		if (files != null) 
		{
			for (int i = 0; i < files.length; i++) 
			{
				//System.out.println("Fichier " + files[i].getAbsolutePath());
				 if (files[i].isDirectory() == false)
					 System.out.println("Try to copy : " + files[i].getAbsolutePath() + " in " +  Project.getInstance().getPath() + "\\IntgAGL\\src\\intgagl\\" + files[i].getName());
					 FileIO.copyPasteFile(files[i].getAbsolutePath(), Project.getInstance().getPath() + "\\IntgAGL\\src\\intgagl\\" + files[i].getName());
			}
		}
		*/
		
		try
		{
			String res = "";
			Runtime run = Runtime.getRuntime();
			String[] cmd =  {cheminNetbeans, "--open", cheminProjet};
			for (String s : cmd)
			{
				res += s+ " ";
			}
			System.out.println(res);
			run.exec(cmd);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	//Launch application
	public static void launchApplication(String path, String file)
	{
		try
		{
			Runtime run = Runtime.getRuntime();
			String[] cmd =  {path, "\"" + file + "\""};
			System.out.println(cmd);
			run.exec(cmd);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	
	//init GUI
	private void initGUI()
	{
		String text = "";
		JLabel jlabel = null;
		
		for (int i = 0; i < getStepList().size(); i++)
		{
			text = getStepList().get(i).getName();
			jlabel = getStepList().get(i).getjLabel();
			
			jlabel.setForeground(Color.LIGHT_GRAY);
			jlabel.setText(text);
		}	
	}
	
	
	//index++
	public void indexPlusPlus()
	{
		index++;
	}
	
	
	//Getter Setter
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	public ArrayList<Step> getStepList() {
		return stepList;
	}

	public void setStepList(ArrayList<Step> stepList) {
		this.stepList = stepList;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getWordPath() {
		return wordPath;
	}

	public void setWordPath(String wordPath) {
		this.wordPath = wordPath;
	}

	public String getWordFile() {
		return wordFile;
	}

	public void setWordFile(String wordFile) {
		this.wordFile = wordFile;
	}
	
	public String getRemoterepositorypath() {
		return remoterepositorypath;
	}

	public void setRemoterepositorypath(String remoterepositorypath) {
		this.remoterepositorypath = remoterepositorypath;
	}

}
