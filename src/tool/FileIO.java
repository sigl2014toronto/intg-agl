package tool;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileIO {
	public static void copyPasteFile (String srcpath, String destpath)
	{	
		File src = new File(srcpath);
		File dest = new File(destpath);
		
		InputStream in;
		
		try {
			in = new BufferedInputStream(new FileInputStream(src));
			OutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
			byte[] buf = new byte[4096];
			int n;
			while ((n=in.read(buf, 0, buf.length)) > 0)
			out.write(buf, 0, n);
			 
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}
	}
	
	public static void copyPasteFolder(String src, String dst)
	{
		File srcFolder = new File(src);
    	File destFolder = new File(dst);
 
    	//make sure source exists
    	if(!srcFolder.exists()){
 
           System.out.println("Directory " + src + " does not exist.");
           //just exit
           System.exit(0);
 
        }else{
 
           try{
        	copyFolder(srcFolder,destFolder);
           }catch(IOException e){
        	e.printStackTrace();
        	//error, just exit
                System.exit(0);
           }
        }
    }
 
    private static void copyFolder(File src, File dest) throws IOException
    {

    		if(src.isDirectory()){
 
    		//if directory not exists, create it
    		if(!dest.exists()){
    		   dest.mkdir();
    		}
 
    		//list all the directory contents
    		String files[] = src.list();
 
    		for (String file : files) {
    		   //construct the src and dest file structure
    		   File srcFile = new File(src, file);
    		   File destFile = new File(dest, file);
    		   //recursive copy
    		   copyFolder(srcFile,destFile);
    		}
 
    	}else{
    		//if file, then copy it
    		//Use bytes stream to support all file types
    		InputStream in = new FileInputStream(src);
    	        OutputStream out = new FileOutputStream(dest); 
 
    	        byte[] buffer = new byte[1024];
 
    	        int length;
    	        //copy the file content in bytes 
    	        while ((length = in.read(buffer)) > 0){
    	    	   out.write(buffer, 0, length);
    	        }
 
    	        in.close();
    	        out.close();
    	}
	}
}
