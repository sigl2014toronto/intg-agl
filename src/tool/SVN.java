/*	cr�er un dossier dans le d�p�t
 * 	command : mkdir [nom du dossier] 
 */

/*	R�cup�rer d�p�t Toronto
 *  command : checkout http://bnf.sigl.epita.fr:81/svn/Toronto  
 */

/*	ajouter tous les nouveaux fichiers
 *  command : add * 
 */

/*	Faire un commit
 *  command : commit -m \"update\" 
 */

package tool;

import java.io.File;
import java.io.IOException;

import org.json.simple.JSONObject;

public class SVN {

	private String username = "";
	private String password = "";

	public SVN()
	{
		// Get svn username and password from config file
		JSONObject jsonSVNObject = Json.loadFromFile("src\\config\\svn.json");
		//**JSONObject jsonSVNObject = Json.loadFromFile("config\\svn.json");
		String svnusername = (String) jsonSVNObject.get("username");
		String svnuserpass = (String) jsonSVNObject.get("password");
		
		this.username = svnusername;
		this.password = svnuserpass;
	}
	
	public void command(String command, String workingdirectory)
	{
		File directory = new File(workingdirectory);
		
		MyRuntime.execSync("svn --non-interactive --no-auth-cache --username " +  this.username + " --password " + this.password + " " + command, null, directory);		
	}
}