package tool;

import java.io.File;
import java.io.IOException;

public class MyRuntime {
	private static Runtime run = Runtime.getRuntime();
	private static Process process = null;
	
	public static void execSync(String command){
		try {
			process = run.exec(command);
			process.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	};
	
	public static void execSync(String command, String[] envp){
		try {
			process = run.exec(command, envp);
			process.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	};
	
	public static void execSync(String command, String[] envp, File dir){
		try {
			process = run.exec(command, envp, dir);
			process.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	};
}