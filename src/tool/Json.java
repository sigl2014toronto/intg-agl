package tool;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/*
 * USE :
 * JSONObject jsonObject = Json.loadFromFile([filename]);
 * String value = (String)jsonObject.get("value");
 */

public class Json {
	
	private static JSONParser parser = new JSONParser();
	
	public static JSONObject loadFromFile(String path){
		try {
			return (JSONObject)parser.parse(new FileReader(path));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
