package aglinterface;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.border.LineBorder;

import tool.SVN;
import model.Project;
import model.Step;

public class Window {

	private JFrame frame;
	/*
	 * Label left menu
	 */
	public static JLabel lblleftmenuconception = new JLabel("");
	public static JLabel lblleftmenuconception2 = new JLabel("");
	public static JLabel lblleftmenurealisation = new JLabel("");
	public static JLabel lblleftmenutest = new JLabel("");
	public static JLabel lblDocumentation = new JLabel("");
	//public static JLabel lblleftmenuconfiguration = new JLabel("");
	
	/*
	 * Label for step description
	 */
	public static JLabel lbltoolname = new JLabel("");
	public static JLabel lblstepname = new JLabel("Par Toronto");
	/*
	 * Text area for step description
	 */
	public static JTextArea taastepdescription = new JTextArea("Cette AGL a �t� pens� par Philadelphia et r�alis� par Toronto, deux groupes SIGL 2014");
	public static JTextArea taastepinstruction = new JTextArea("Vous pouvez � tout moment ouvrir Word via le menu\nPour commencer le d�but du projet " + Project.getInstance().getName() + " cliquez sur suivant");

	/**
	 * Launch the application.
	 */
	public static void open() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setResizable(false);
		frame.setBounds(100, 100, 700, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel lblleftmenudocumentation = new JPanel();
		lblleftmenudocumentation.setBackground(UIManager.getColor("Label.background"));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);

		JLabel lblInstruction = new JLabel("Instruction");

		JButton btnnext = new JButton("Suivant");
		btnnext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Project project = Project.getInstance();

				// Changer les valeurs de la fen�tre
				if (project.getIndex() != project.getStepList().size()) {

					int index = project.getIndex();
					Step step = project.getStepList().get(index);
					String toolPath = step.getPathExecTool();
					String file = step.getPathFile();
					
					// Grisage de toutes les �tapes
					for (int i = 0; i < project.getStepList().size(); i++) {
						project.getStepList().get(i).getjLabel()
								.setForeground(Color.LIGHT_GRAY);
					}

					// Mise � jour des informations de la phase
					step.getjLabel().setForeground(Color.BLACK);
					lbltoolname.setText(step.getToolName());
					lblstepname.setText(step.getName());
					taastepdescription.setText(step.getDescription());
					taastepinstruction.setText(step.getInstruction());
					
					if (toolPath != null && toolPath != "" && file != null && file != "") 
					{
						if (step.getIdSoft() == 1)  //DiaToCode
 						{
							javax.swing.JOptionPane.showMessageDialog(null, "Path : " + step.getPathExecTool() + "\nDia file  " + step.getPathFile() + "\nJava Folder : " + project.getPath()+"\\JAVA");
							Project.launchDiaCode(step.getPathExecTool(), step.getPathFile(), Project.getInstance().getPath() + "\\IntgAGL\\src\\intgagl\\");//project.getPath()+"\\JAVA");
						}
						else if (step.getIdSoft() == 2) //netbeans
						{
							javax.swing.JOptionPane.showMessageDialog(null, "Netbeans path : " + step.getPathExecTool() + "\nDirectory : " + step.getPathFile());
							Project.launchNetBeans(step.getPathExecTool(), step.getPathFile());
						}
						else
						{
						   javax.swing.JOptionPane.showMessageDialog(null, "Lancement : \nPath: " + toolPath + "\nFile : "	+ file);
						   Project.launchApplication(step.getPathExecTool(), file);
						}
						//Push sur le SVN
						System.out.println("Appel de svn");
						SVN svn = new SVN();
						svn.command("add *", project.getRemoterepositorypath());
						svn.command("commit -m 'update'", project.getRemoterepositorypath());
						System.out.println("Fin Appel de svn");
					}

					project.indexPlusPlus();
				}
			}
		});

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(UIManager.getColor("CheckBox.light")));

		JLabel lblDescriptionDeLa = new JLabel("AGL Philadelphia");

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addComponent(lblleftmenudocumentation,
												GroupLayout.PREFERRED_SIZE,
												185, GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																scrollPane,
																Alignment.TRAILING,
																GroupLayout.DEFAULT_SIZE,
																481,
																Short.MAX_VALUE)
														.addComponent(
																btnnext,
																Alignment.TRAILING)
														.addComponent(
																panel_1,
																Alignment.TRAILING,
																GroupLayout.DEFAULT_SIZE,
																481,
																Short.MAX_VALUE)
														.addGroup(
																Alignment.TRAILING,
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				lblDescriptionDeLa,
																				GroupLayout.PREFERRED_SIZE,
																				144,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(18)
																		.addComponent(
																				lblstepname,
																				GroupLayout.PREFERRED_SIZE,
																				120,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(36)
																		.addComponent(
																				lbltoolname,
																				GroupLayout.PREFERRED_SIZE,
																				163,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(
																lblInstruction))
										.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout
				.createParallelGroup(Alignment.TRAILING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addGap(10)
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(
														lblDescriptionDeLa)
												.addComponent(lbltoolname)
												.addComponent(lblstepname))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel_1,
										GroupLayout.DEFAULT_SIZE, 192,
										Short.MAX_VALUE)
								.addGap(16)
								.addComponent(lblInstruction)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(scrollPane,
										GroupLayout.PREFERRED_SIZE, 234,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(btnnext).addGap(25))
				.addComponent(lblleftmenudocumentation,
						GroupLayout.DEFAULT_SIZE, 551, Short.MAX_VALUE));

		JScrollPane scrollPane_1 = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(gl_panel_1.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panel_1
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE,
								449, Short.MAX_VALUE).addContainerGap()));
		gl_panel_1.setVerticalGroup(gl_panel_1.createParallelGroup(
				Alignment.LEADING).addGroup(
				Alignment.TRAILING,
				gl_panel_1
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE,
								147, Short.MAX_VALUE).addContainerGap()));

		taastepdescription.setEnabled(true);
		taastepdescription.setEditable(false);
		taastepdescription.setBackground(UIManager.getColor("CheckBox.light"));
		scrollPane_1.setViewportView(taastepdescription);
		panel_1.setLayout(gl_panel_1);

		taastepinstruction.setBackground(UIManager.getColor("CheckBox.light"));
		taastepinstruction.setEditable(false);
		scrollPane.setViewportView(taastepinstruction);

		lblleftmenuconception.setForeground(Color.LIGHT_GRAY);
		lblleftmenuconception2.setForeground(Color.LIGHT_GRAY);
		lblleftmenurealisation.setForeground(Color.LIGHT_GRAY);
		lblleftmenutest.setForeground(Color.LIGHT_GRAY);
		lblDocumentation.setForeground(Color.LIGHT_GRAY);
//		lblleftmenuconfiguration.setForeground(Color.LIGHT_GRAY);

//		lblleftmenuconfiguration.setToolTipText("");
		GroupLayout gl_lblleftmenudocumentation = new GroupLayout(
				lblleftmenudocumentation);
		gl_lblleftmenudocumentation
				.setHorizontalGroup(gl_lblleftmenudocumentation
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_lblleftmenudocumentation
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_lblleftmenudocumentation
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblleftmenuconception)
														.addComponent(
																lblleftmenuconception2)
														.addComponent(
																lblleftmenurealisation)
														.addComponent(
																lblleftmenutest)
														.addComponent(
																lblDocumentation)
														/*.addComponent(
																lblleftmenuconfiguration)*/)
										.addContainerGap(25, Short.MAX_VALUE)));
		gl_lblleftmenudocumentation
				.setVerticalGroup(gl_lblleftmenudocumentation
						.createParallelGroup(Alignment.LEADING).addGroup(
								gl_lblleftmenudocumentation
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(lblleftmenuconception)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblleftmenuconception2)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblleftmenurealisation)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblleftmenutest)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblDocumentation)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										//.addComponent(lblleftmenuconfiguration)
										.addContainerGap(257, Short.MAX_VALUE)));
		lblleftmenudocumentation.setLayout(gl_lblleftmenudocumentation);
		frame.getContentPane().setLayout(groupLayout);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnOutilsTransverse = new JMenu("Outils transverse");
		menuBar.add(mnOutilsTransverse);

		JMenuItem mntmWord = new JMenuItem("Word");
		mntmWord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Project.launchWord();
			}
		});
		mnOutilsTransverse.add(mntmWord);

		JMenuItem mntmOsrmt = new JMenuItem("OSRMT");
		mnOutilsTransverse.add(mntmOsrmt);

		JMenuItem mntmSubversion = new JMenuItem("Subversion");
		mnOutilsTransverse.add(mntmSubversion);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		JMenuItem mntmNewMenuItem = new JMenuItem(
				"Dossier de D�marche M�thodologiques ");
		mnAide.add(mntmNewMenuItem);
	}
	
	/*
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}*/
}
