package aglinterface;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import tool.SVN;
import model.Project;

public class MainWindow {

	private JFrame frmAgl;
	private JTextField iptprojectpath;
	private JTextField iptprojectname;

	/**
	 * Launch the application.
	 */
	public static void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmAgl.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAgl = new JFrame();
		frmAgl.setTitle("AGL");
		frmAgl.setBounds(100, 100, 500, 300);
		frmAgl.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JButton btncreateproject = new JButton("Valider");
		btncreateproject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				/*
				if (Project.getInstance().getName() == null || Project.getInstance().getName().isEmpty() )
				{
					javax.swing.JOptionPane.showMessageDialog(null,"Le nom doit �tre renseign�"); 	
					return;
				}
				else if (Project.getInstance().getPath() == null || Project.getInstance().getPath().isEmpty())
				{
					javax.swing.JOptionPane.showMessageDialog(null,"Le chemin doit �tre inscrit"); 						
					return;
				}
				*/

				//Sauvegarde des informations
				String name = iptprojectname.getText();
				String path = iptprojectpath.getText();
				
				new File(path + "\\" + name).mkdir();
				
				SVN svn = new SVN();
				svn.command("checkout http://bnf.sigl.epita.fr:81/svn/Toronto", path + "\\" + name);
				
				Project.getInstance().setName(name);
				Project.getInstance().setPath(path + "\\" + name + "\\Toronto\\INTG\\AGL\\" + name);
				Project.getInstance().setRemoterepositorypath(path + "\\" + name + "\\Toronto\\INTG\\AGL");
			
				javax.swing.JOptionPane.showMessageDialog(null,"Caract�ristiques retenues : \n" + "Chemin : " + Project.getInstance().getPath()  + "\nNom Projet : " + Project.getInstance().getName());
				
				Project.createEnvironnement();
				
				// ouvrir fen�tre principale
				
				Project.getInstance().setStepListInfo();
				Window.open();
				frmAgl.dispose();
			}
		});
		
		iptprojectpath = new JTextField();
		iptprojectpath.setColumns(10);
		
		iptprojectname = new JTextField();
		iptprojectname.setColumns(10);
		
		JLabel lblNomDuProjet = new JLabel("Nom du projet");
		
		JLabel lblCheminDuProjet = new JLabel("Chemin du projet");
		
		JButton btnchooseprojectpath = new JButton("...");
		btnchooseprojectpath.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				// ouvrir menu pour choisir chemin dossier

				JFileChooser dialog = new JFileChooser();
				dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				
				if (dialog.showSaveDialog(frmAgl) == JFileChooser.APPROVE_OPTION) 
				{
					iptprojectpath.setText(dialog.getSelectedFile().toString());
				}
			}
		});
		GroupLayout groupLayout = new GroupLayout(frmAgl.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(203)
					.addComponent(btncreateproject)
					.addContainerGap(216, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(85, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(lblNomDuProjet)
							.addComponent(iptprojectname, GroupLayout.PREFERRED_SIZE, 319, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(lblCheminDuProjet, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
							.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
								.addComponent(iptprojectpath, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(btnchooseprojectpath, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE))))
					.addGap(80))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(65, Short.MAX_VALUE)
					.addComponent(lblNomDuProjet)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(iptprojectname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(lblCheminDuProjet)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnchooseprojectpath)
						.addComponent(iptprojectpath, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addComponent(btncreateproject)
					.addGap(34))
		);
		frmAgl.getContentPane().setLayout(groupLayout);
	}
}
