package artifact;

import java.io.PrintWriter;

public class Requirement extends Artifact
{
	private String context;
	private String description;
	private String goal;
	private String origin;
	private String postcondition;
	private String precondition;
	private String rationale;
	
	public Requirement(String name)
	{
		this.artifactRefDisplay = "Requirement";
		this.artifactRefId = "131";
		this.artifactName = name;
		this.context = "";
		this.description = "";
		this.goal = "";
		this.origin = "";
		this.postcondition = "";
		this.precondition = "";
		this.rationale = "";
	}
	
	public Requirement(String name, String context, String description, String goal, String origin, String postcondition, String precondition, String rationale)
	{
		this.artifactRefDisplay = "Requirement";
		this.artifactRefId = "131";
		this.artifactName = name;
		this.context = context;
		this.description = description;
		this.goal = goal;
		this.origin = origin;
		this.postcondition = postcondition;
		this.precondition = precondition;
		this.rationale = rationale;
	}
	
	public void write (PrintWriter writer)
	{
		writer.println("  <void property=\"context\">");
		writer.println("   <string>" + context + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"description\">");
		writer.println("   <string>" + description + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"goal\">");
		writer.println("   <string>" + goal + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"origin\">");
		writer.println("   <string>" + origin + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"postcondition\">");
		writer.println("   <string>" + postcondition + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"precondition\">");
		writer.println("   <string>" + precondition + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"rationale\">");
		writer.println("   <string>" + rationale + "</string>");
		writer.println("  </void>");
	}

	public String getContext()
	{
		return context;
	}
	public void setContext(String context)
	{
		this.context = context;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGoal()
	{
		return goal;
	}
	public void setGoal(String goal)
	{
		this.goal = goal;
	}
	public String getOrigin()
	{
		return origin;
	}
	public void setOrigin(String origin)
	{
		this.origin = origin;
	}
	public String getPostcondition()
	{
		return postcondition;
	}
	public void setPostcondition(String postcondition)
	{
		this.postcondition = postcondition;
	}
	public String getPrecondition()
	{
		return precondition;
	}
	public void setPrecondition(String precondition)
	{
		this.precondition = precondition;
	}
	public String getRationale()
	{
		return rationale;
	}
	public void setRationale(String rationale)
	{
		this.rationale = rationale;
	}
}
