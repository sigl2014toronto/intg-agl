package artifact;

import java.io.PrintWriter;

public class Implementation extends Artifact
{
	private String description;

	public Implementation(String name)
	{
		this.artifactRefDisplay = "Implementation";
		this.artifactRefId = "132";
		this.artifactName = name;
		this.description = "";
	}
	
	public Implementation(String name, String description)
	{
		this.artifactRefDisplay = "Implementation";
		this.artifactRefId = "132";
		this.artifactName = name;
		this.description = description;
	}
	
	public void write(PrintWriter writer)
	{
		writer.println("  <void property=\"description\">");
		writer.println("   <string>" + description + "</string>");
		writer.println("  </void>");
	}
	
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
}
