package artifact;

//import parsers.DiaParser.DiaFileParser;
//import parsers.DiaParser.bo.DiaFile;

import java.io.PrintWriter;

public class Design extends Artifact
{
	private String description;
	
	public Design(String name)
	{
		this.artifactRefDisplay = "Design";
		this.artifactRefId = "130";
		this.artifactName = name;
		this.description = "";
	}
	
	public Design(String name, String description)
	{
		this.artifactRefDisplay = "Design";
		this.artifactRefId = "130";
		this.artifactName = name;
		this.description = description;
	}
	
	public void write(PrintWriter writer) 
	{
        //DiaFile diaFile = DiaFileParser.parse(this.artifactName);

		writer.println("<void property=\"description\">");
		//writer.println("<string>" + diaFile.getDescription() + "</string>");
		writer.println("</void>");
	}

	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
}
