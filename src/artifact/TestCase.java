package artifact;

import java.io.PrintWriter;

public class TestCase extends Artifact
{
	private String context;
	private String description;
	private String goal;
	private String postcondition;
	private String precondition;
	
	public TestCase(String name)
	{
		this.artifactRefDisplay = "TestCase";
		this.artifactRefId = "133";
		this.artifactName = name;
		this.context = "";
		this.description = "";
		this.goal = "";
		this.postcondition = "";
		this.precondition = "";
	}
	
	public TestCase (String name, String context, String description, String goal, String postcondition, String precondition)
	{
		this.artifactRefDisplay = "TestCase";
		this.artifactRefId = "133";
		this.artifactName = name;
		this.context = context;
		this.description = description;
		this.goal = goal;
		this.postcondition = postcondition;
		this.precondition = precondition;	
	}
	
	public void write(PrintWriter writer)
	{
		writer.println("  <void property=\"context\">");
		writer.println("   <string>" + context + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"description\">");
		writer.println("   <string>" + description + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"goal\">");
		writer.println("   <string>" + goal + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"postcondition\">");
		writer.println("   <string>" + postcondition + "</string>");
		writer.println("  </void>");
		writer.println("  <void property=\"precondition\">");
		writer.println("   <string>" + precondition + "</string>");
		writer.println("  </void>");
	}
	
	public String getContext()
	{
		return context;
	}
	public void setContext(String context)
	{
		this.context = context;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getGoal()
	{
		return goal;
	}
	public void setGoal(String goal)
	{
		this.goal = goal;
	}
	public String getPostcondition()
	{
		return postcondition;
	}
	public void setPostcondition(String postcondition)
	{
		this.postcondition = postcondition;
	}
	public String getPrecondition()
	{
		return precondition;
	}
	public void setPrecondition(String precondition)
	{
		this.precondition = precondition;
	}
}
