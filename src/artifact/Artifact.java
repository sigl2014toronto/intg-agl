package artifact;

import java.io.PrintWriter;

public abstract class Artifact
{
	protected String artifactRefDisplay;
	protected String artifactName;
	protected String artifactRefId;
	
	public Artifact()
	{
	}
	public Artifact(String artifactRefDisplay, String artifactName)
	{
		this.artifactRefDisplay = artifactRefDisplay;
		this.artifactName = artifactName;
	}

	public abstract void write(PrintWriter writer);

	public String getArtifactRefDisplay()
	{
		return artifactRefDisplay;
	}
	public void setArtifactRefDisplay(String artifactRefDisplay)
	{
		this.artifactRefDisplay = artifactRefDisplay;
	}
	public String getArtifactName()
	{
		return artifactName;
	}
	public void setArtifactName(String artifactName)
	{
		this.artifactName = artifactName;
	}
	public String getArtifactRefId()
	{
		return artifactRefId;
	}
	public void setArtifactRefId(String artifactRefId)
	{
		this.artifactRefId = artifactRefId;
	}
}
