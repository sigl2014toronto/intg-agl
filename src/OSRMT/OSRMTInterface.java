package OSRMT;

import artifact.Artifact;
import model.Project;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class OSRMTInterface
{
	private PrintWriter writer;
	private File path;

	public OSRMTInterface()
	{
		path = new File(Project.getInstance().getPath() + "\\OSRMT\\");
	}

	public void write(Artifact artifact)
	{		
		try
		{
			String filepath = Project.getInstance().getPath() + "\\OSRMT\\" + artifact.getArtifactName() + ".out.xml";
			File file = new File(filepath);
			
			File[] files = path.listFiles();
			for (File f : files)
				if (f.getAbsoluteFile().equals(filepath))
				{
					f.delete();
					break;
				}
				
			if (file.createNewFile())
			{
				writer = new PrintWriter(new FileWriter(file));
				
				writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				writer.println("<java version=\"1.7.0_21\" class=\"java.beans.XMLDecoder\">");
				
				writer.println(" <object class=\"com.osrmt.modellibrary.reqmanager.ArtifactModel\">");
				writer.println("  <void property=\"artifactName\">");
				writer.println("   <string>" + artifact.getArtifactName() + "</string>");
				writer.println("  </void>");
				writer.println("  <void property=\"artifactRefDisplay\">");
				writer.println("   <string>" + artifact.getArtifactRefDisplay() + "</string>");
				writer.println("  </void>");
				writer.println("  <void property=\"artifactRefId\">");
				writer.println("   <int>" + artifact.getArtifactRefId() + "</int>");
				writer.println("  </void>");
				
				artifact.write(writer);
				
				writer.println(" </object>");
				
				writer.println("</java>");
				writer.flush();
				writer.close();
			}
			
			System.out.println("Op�ration termin�e");
		}
		catch (NullPointerException | IOException a)
		{
			a.printStackTrace();
			System.out.println("Pointeur null");
		}
		
	}
}