package parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import OSRMT.OSRMTInterface;
import artifact.Artifact;
import artifact.Implementation;

public class ImplementationParser
{
	public static void parseFolder(String path)
	{
		ArrayList<Artifact> artifacts = new ArrayList<Artifact>();
		
		OSRMTInterface inter = new OSRMTInterface();
		Implementation i;
		
		File folder = new File(path);
		for (File f : folder.listFiles())
		{
			if (f.isDirectory())
				parseFolder(f.getPath());
			else if (f.getName().substring(f.getName().lastIndexOf(".")).equals(".java"))
			{
				i = parseFile(f.getPath());
				artifacts.add(i);
			}
		}
		
		for (Artifact a : artifacts)
			inter.write(a);
	}
	
	@SuppressWarnings("finally")
	private static Implementation parseFile(String path)
	{
		Implementation result = null;
		String name = "";
		String description = "";
		
		try
		{
			name = path.substring(path.lastIndexOf("\\") + 1, path.lastIndexOf("."));
			
			InputStream ips = new FileInputStream(path); 
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			
			String line;
			while ((line = br.readLine()) != null)
				description += line + "\n";
			
			result = new Implementation(name, description);
			
			br.close(); 
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			return result;
		}
		
	}
}
