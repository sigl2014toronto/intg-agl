package parsers.DiaParser;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import parsers.DiaParser.bo.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Author:  boltz_j
 * Date:    20/10/13 @ 04:21
 */

public class DiaFileParser {

    static private Document domParserInit(String filename) {
        Document document = null;
        try {
            // Get DOM Parser
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();

            // Read the XML File
            File file= new File(filename);
            document = docBuilder.parse(file);

        } catch (FileNotFoundException e) {
            System.err.println("File not found : ".concat(filename));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    static private void parseGeneralization() {
        // FIXME
    }

    static public DiaFile parse(String filename) {

        DiaFile diaFile = new DiaFile();

        // Get the document
        Document document = DiaFileParser.domParserInit(filename);

        // Handle error on parsing
        if (document == null) {
            System.err.println("Fail to parse ".concat(filename));
            return diaFile;
        }

        NodeList nodeList = document.getElementsByTagName("dia:object");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node elt = nodeList.item(i);

            // --- New Class ---
            if (elt.getAttributes().getNamedItem("type").getNodeValue().equals("UML - Class"))
            {
                // Create a new class and link it to the file
                Integer classId = Integer.parseInt(elt.getAttributes().getNamedItem("id").getNodeValue().substring(1));
                DiaClass diaClass = new DiaClass(classId);
                diaFile.addClass(diaClass);

                NodeList list = elt.getChildNodes();
                for (int j = 0; j < list.getLength(); j++) {
                    Node attr = list.item(j);

                    if (attr.getNodeName().equals("dia:attribute"))
                    {
                        // Get the class name
                        if (attr.getAttributes().getNamedItem("name").getNodeValue().equals("name")) {
                            String name = DiaFileParser.trim(attr.getChildNodes().item(1).getTextContent());
                            diaClass.setName(name);
                        }

                        // Get class stereotype
                        else if (attr.getAttributes().getNamedItem("name").getNodeValue().equals("stereotype")) {
                            String stereotype = DiaFileParser.trim(attr.getChildNodes().item(1).getTextContent());
                            diaClass.setStereotype(stereotype);
                        }

                        // Get class comment
                        else if (attr.getAttributes().getNamedItem("name").getNodeValue().equals("comment")) {
                            String comment = DiaFileParser.trim(attr.getChildNodes().item(1).getTextContent());
                            diaClass.setComment(comment);
                        }

                        // is Abstract ?
                        else if (attr.getAttributes().getNamedItem("name").getNodeValue().equals("abstract")) {
                            Boolean isAbstract = Boolean.parseBoolean(attr.getChildNodes().item(1).getAttributes().getNamedItem("val").getNodeValue());
                            diaClass.setAbstract(isAbstract);
                        }

                        // Get class attributes
                        else if (attr.getAttributes().getNamedItem("name").getNodeValue().equals("attributes"))
                        {
                            NodeList attributes = attr.getChildNodes();
                            for (int k = 0; k < attributes.getLength(); k++) {
                                Node classAttr = attributes.item(k);

                                if (classAttr.getNodeName().equals("dia:composite") && classAttr.getAttributes().getNamedItem("type").getNodeValue().equals("umlattribute")) {
                                    // Create a new class attribute
                                    DiaAttribute classAttribute = new DiaAttribute();
                                    diaClass.getAttributes().add(classAttribute);

                                    NodeList classAttrInfo = classAttr.getChildNodes();
                                    for (int l = 0; l < classAttrInfo.getLength(); l++) {
                                        Node classAttributeAttr = classAttrInfo.item(l);

                                        if (classAttributeAttr.getNodeName().equals("dia:attribute")) {
                                            if (classAttributeAttr.getAttributes().getNamedItem("name").getNodeValue().equals("name")) {
                                                String name = DiaFileParser.trim(classAttributeAttr.getChildNodes().item(1).getTextContent());
                                                classAttribute.setName(name);
                                            }

                                            else if (classAttributeAttr.getAttributes().getNamedItem("name").getNodeValue().equals("type")) {
                                                String type = DiaFileParser.trim(classAttributeAttr.getChildNodes().item(1).getTextContent());
                                                classAttribute.setType(type);
                                            }

                                            else if (classAttributeAttr.getAttributes().getNamedItem("name").getNodeValue().equals("visibility")) {
                                                Integer visibility = Integer.parseInt(classAttributeAttr.getChildNodes().item(1).getAttributes().getNamedItem("val").getNodeValue());
                                                classAttribute.setVisibility(visibility);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Get class operations
                        else if (attr.getAttributes().getNamedItem("name").getNodeValue().equals("operations"))
                        {
                            NodeList operations = attr.getChildNodes();

                            for (int k = 0; k < operations.getLength(); k++) {
                                Node composite = operations.item(k);

                                if (composite.getNodeName().equals("dia:composite") && composite.getAttributes().getNamedItem("type").getNodeValue().equals("umloperation"))
                                {
                                    // Create a new operation and attach it to the class
                                    DiaOperation diaOperation = new DiaOperation();
                                    diaClass.getOperations().add(diaOperation);

                                    NodeList attributes = composite.getChildNodes();

                                    for (int l = 0; l < attributes.getLength(); l++)
                                    {
                                        Node operationAttr = attributes.item(l);

                                        if (operationAttr.getNodeName().equals("dia:attribute"))
                                        {
                                            // Get operation name
                                            if (operationAttr.getAttributes().getNamedItem("name").getNodeValue().equals("name")) {
                                                String name = DiaFileParser.trim(operationAttr.getChildNodes().item(1).getTextContent());
                                                diaOperation.setName(name);
                                            }

                                            // Get operation type
                                            else if (operationAttr.getAttributes().getNamedItem("name").getNodeValue().equals("type")) {
                                                String type = DiaFileParser.trim(operationAttr.getChildNodes().item(1).getTextContent());
                                                diaOperation.setType(type);
                                            }

                                            // Get visibilty
                                            else if (operationAttr.getAttributes().getNamedItem("name").getNodeValue().equals("visibility")) {
                                                Integer visibility = Integer.parseInt(operationAttr.getChildNodes().item(1).getAttributes().getNamedItem("val").getNodeValue());
                                                diaOperation.setVisibility(visibility);
                                            }

                                            // Is Abstract
                                            else if (operationAttr.getAttributes().getNamedItem("name").getNodeValue().equals("abstract")) {
                                                Boolean isAbstract = Boolean.parseBoolean(operationAttr.getChildNodes().item(1).getAttributes().getNamedItem("val").getNodeValue());
                                                diaOperation.setAbstract(isAbstract);
                                            }

                                            // Set parameters
                                            else if (operationAttr.getAttributes().getNamedItem("name").getNodeValue().equals("parameters")) {
                                                NodeList parameters = operationAttr.getChildNodes();

                                                for (int m = 0; m < parameters.getLength(); m++) {
                                                    Node parameter = parameters.item(m);

                                                    if (parameter.getNodeName().equals("dia:composite") && parameter.getAttributes().getNamedItem("type").getNodeValue().equals("umlparameter")) {
                                                        // Create new parameter and add it to the operation
                                                        DiaParameter diaParameter = new DiaParameter();
                                                        diaOperation.getParameters().add(diaParameter);

                                                        // Parse the parameter details
                                                        NodeList parameterAttributes = parameter.getChildNodes();
                                                        for (int n = 0; n < parameterAttributes.getLength(); n++) {
                                                            Node parameterAttr = parameterAttributes.item(n);

                                                            if (parameterAttr.getNodeName().equals("dia:attribute")) {
                                                                if (parameterAttr.getAttributes().getNamedItem("name").getNodeValue().equals("name")) {
                                                                    String name = DiaFileParser.trim(parameterAttr.getChildNodes().item(1).getTextContent());
                                                                    diaParameter.setName(name);
                                                                }
                                                                else if (parameterAttr.getAttributes().getNamedItem("name").getNodeValue().equals("type")) {
                                                                    String type = DiaFileParser.trim(parameterAttr.getChildNodes().item(1).getTextContent());
                                                                    diaParameter.setType(type);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            // --- Generalization ---
            else if (elt.getAttributes().getNamedItem("type").getNodeValue().equals("UML - Generalization")) {
                // Create a new class and link it to the file
                Integer classId = Integer.parseInt(elt.getAttributes().getNamedItem("id").getNodeValue().substring(1));

                NodeList list = elt.getChildNodes();

                for (int j = 0; j < list.getLength(); j++) {
                    Node attr = list.item(j);

                    if (attr.getNodeName().equals("dia:connections"))
                    {
                        if (attr.getChildNodes().item(1).getAttributes().getNamedItem("handle").getNodeValue().equals("0") && attr.getChildNodes().item(3).getAttributes().getNamedItem("handle").getNodeValue().equals("1")) {
                            // Get Ids
                            Integer superClassId = Integer.parseInt(attr.getChildNodes().item(1).getAttributes().getNamedItem("to").getNodeValue().substring(1));
                            Integer subClassId = Integer.parseInt(attr.getChildNodes().item(3).getAttributes().getNamedItem("to").getNodeValue().substring(1));

                            // Get classes
                            DiaClass diaSuperClass = diaFile.getClass(superClassId);
                            DiaClass diaSubClass = diaFile.getClass(subClassId);

                            // Link
                            diaSubClass.setSuperClass(diaSuperClass);
                            diaSuperClass.getSubclasses().add(diaSubClass);
                        }
                    }
                }

            }
        }

        return diaFile;
    }

    static private String trim(String s) {
        return s.substring(1, s.length() - 1);
    }

}
