package parsers.DiaParser.bo;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: boltz_j
 * Date: 12/10/13
 * Time: 16:19
 * To change this template use File | Settings | File Templates.
 */
public class DiaOperation {
    private String name = null;
    private String type = null;
    private Integer visibility = null;
    private Boolean isAbstract = null;

    private List<DiaParameter> parameters = null;

    /**
     * @constructor
     */
    public DiaOperation() {
        parameters =  new LinkedList<DiaParameter>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public List<DiaParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<DiaParameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(DiaParameter parameter) {
        this.parameters.add(parameter);
    }

    public void print() {
        // FIXME : Print()
        if (this.isAbstract) {
            System.out.print("Abstract ");
        }

        System.out.print(this.visibility);
        System.out.print("\t");
        System.out.print(this.type);
        System.out.print("\t");
        System.out.print(this.name);
        System.out.print("(");
        for (DiaParameter diaParameter : this.parameters) {
            diaParameter.print();
        }
        System.out.println(")");
    }

    public Boolean getAbstract() {
        return isAbstract;
    }

    public void setAbstract(Boolean anAbstract) {
        isAbstract = anAbstract;
    }

    public String getDescription() {
        String res = null;

        switch (this.visibility) {
            case 0 : res = new String("\t\tpublic   \t");
                break;
            case 1 : res = new String("\t\tprivate  \t");
                break;
            case 2 : res = new String("\t\tprotected\t");
                break;
            default: System.err.println("INVALID VISIBILITY");
        }

        res = res.concat(this.type).concat("\t").concat(this.name).concat("(");
        for (DiaParameter parameter : parameters) {
            res = res.concat(parameter.getDescription());

            if (parameters.indexOf(parameter) + 1 < parameters.size()) {
                res = res.concat(", ");
            }
        }
        res = res.concat(")");

        return res;
    }

    private String getVisibilityString() {
        switch (this.visibility) {
            case 0 :
                return "public";
            case 1 :
                return "private";
            case 2 :
                return "protected";
            default:
                System.err.println("UNKNOWN VISIBILITY VALUE : ".concat(this.toString()));
                return null;
        }
    }

    public String getXML(String prefix, String tab) {
        String result = new String();

        // Open Tag
        result += prefix;
        result += "<method";
        result += (" visibility=\"").concat(this.getVisibilityString()).concat("\"");
        result += (null != type && !type.isEmpty()) ? " type=\"".concat(type).concat("\"") : "";
        result += (null != name && !name.isEmpty()) ? " name=\"".concat(name).concat("\"") : "";
        result += ">\n";

        // Parmeters :
        if (this.parameters.size() > 0) {

            result += prefix.concat(tab).concat("<parameters>\n");

            // TODO : list parameters HERE
            for (DiaParameter parameter : parameters) {
                result += parameter.getXML(prefix.concat(tab).concat(tab), tab);
            }

            result += prefix.concat(tab).concat("</parameters>\n");
        }

        // Close tag
        result += prefix;
        result += "</method>\n";

        return result;
    }

    public String getXML() {
        return this.getXML("", "");
    }
}
