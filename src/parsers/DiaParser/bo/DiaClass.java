package parsers.DiaParser.bo;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author:  boltz_j
 * Date:    12/10/13 @ 13:36
 */
public class DiaClass {
    private Integer id = null;
    private String name = null;
    private String stereotype = null;
    private String comment = null;
    private Boolean isAbstract = null;

    private List<DiaAttribute> attributes = null;
    private List<DiaOperation> operations = null;

    // Generalization
    private DiaClass superClass = null;
    private List<DiaClass> subclasses = null;


    /**
     *
     * @param id    id of the class set in dia file
     */
    public DiaClass(Integer id) {
        this.id = id;
        attributes = new LinkedList<>();
        operations = new LinkedList<>();
        subclasses = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStereotype() {
        return stereotype;
    }

    public void setStereotype(String stereotype) {
        this.stereotype = stereotype;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getAbstract() {
        return isAbstract;
    }

    public void setAbstract(Boolean anAbstract) {
        isAbstract = anAbstract;
    }

    public List<DiaAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<DiaAttribute> attributes) {
        this.attributes = attributes;
    }

    public List<DiaOperation> getOperations() {
        return operations;
    }

    public void setOperations(List<DiaOperation> operations) {
        this.operations = operations;
    }

    /**
     *  @param      attribute to add to the class attributes
     */
    public void addAttribute(DiaAttribute attribute) {
        this.attributes.add(attribute);
    }

    /**
     *  @param      operation to add to the class operations
     */
    public void addOperation(DiaOperation operation) {
        this.operations.add(operation);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String toString() {
        String res = new String("");

        res = res.concat("<").concat(this.name).concat(">\n");

        res.concat("\nAttributs:\n");
        for (DiaAttribute diaAttribute : this.attributes) {
            res = res.concat(diaAttribute.toString()).concat(",");
        }

        res.concat("\nMethods:\n");
        for (DiaOperation diaOperation: this.operations) {
            res = res.concat(diaOperation.toString()).concat(",");
        }

        return res;
    }

    public void print() {
        System.out.println("==================BEGIN=================");
        System.out.println(" id :\t".concat(this.id.toString()));
        System.out.println(" name :\t".concat(this.name));
        if (this.superClass != null)
            System.out.println(" super :\t".concat(this.superClass.getName()));
        System.out.println(" abstract :\t".concat(this.isAbstract.toString()));
        System.out.println(" stereotype :\t".concat(this.stereotype));
        System.out.println(" comment :\t".concat(this.comment));
        System.out.println("----------------------------------------");
        for (DiaAttribute diaAttribute : this.attributes) {
            diaAttribute.print();
        }
        System.out.println("----------------------------------------");
        for (DiaOperation diaOperation: this.operations) {
            diaOperation.print();
        }
        System.out.println("===================END===================");
    }

    public DiaClass getSuperClass() {
        return superClass;
    }

    public void setSuperClass(DiaClass superClass) {
        this.superClass = superClass;
    }

    public List<DiaClass> getSubclasses() {
        return subclasses;
    }

    public void setSubclasses(List<DiaClass> subclasses) {
        this.subclasses = subclasses;
    }

    public String getDescription() {
        String res = new String("");

        res = res.concat("<").concat(this.name).concat(">\n");

        res = res.concat("\t<Attributs>\n");
        for (DiaAttribute diaAttribute : this.attributes) {
            res = res.concat(diaAttribute.toString()).concat(",\n");
        }
        res = res.concat("\t</Attributs>\n");

        res = res.concat("\t<Methods>\n");
        for (DiaOperation diaOperation: this.operations) {
            res = res.concat(diaOperation.getDescription()).concat(",\n");
        }
        res = res.concat("\t</Methods>\n");

        return res;
    }

    public String getXML() {
        return this.getXML("", "");
    }

    public String getXML(String prefix, String tab) {

        String result = new String();

        result += prefix;
        result += "<class name=\"";
        result += this.getName().concat("\" ");
        result += (this.getAbstract() ? "abstract=\"true\" " : "");
        result += (this.getSuperClass() != null ? "superclass=\"".concat(this.getSuperClass().getName()).concat("\"") : "");
        result += ">\n";


        result += prefix;
        result += tab;
        result += "<Attributs>\n";

        for (DiaAttribute diaAttribute : this.attributes) {
            result += diaAttribute.getXML(prefix.concat(tab).concat(tab), tab);
        }

        result += prefix;
        result += tab;
        result += "</Attributs>\n";

        result += prefix;
        result += tab;
        result += "<Methods>\n";
        for (DiaOperation diaOperation: this.operations) {
            result += diaOperation.getXML(prefix.concat(tab).concat(tab), tab);
        }

        result += prefix;
        result += tab;
        result = result.concat("</Methods>\n");

        result += prefix;
        result = result.concat("</class>\n");

        return result;
    }
}
