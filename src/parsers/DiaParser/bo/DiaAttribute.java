package parsers.DiaParser.bo;

/**
 * Created with IntelliJ IDEA.
 * Author:  boltz_j
 * Date:    12/10/13 @ 16:18
 */
public class DiaAttribute {
    private String name = null;
    private String type = null;
    private Integer visibility = null;

    /**
     *
     * @return  return the name of the class attribute
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name  set the name of the class attribute
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return  return the type of a class attribute
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type  Set the type of a class attribute
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return      Return the visibility of the attribute : 0 = public, 1 = private, 2 = protected
     */
    public Integer getVisibility() {
        return visibility;
    }

    /**
     *
     * @param visibility Set the visibility of the class attribute (0 = public, 1 = private, 2 = protected)
     */
    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public String toString() {
        String res = null;
        switch (this.visibility) {
            case 0 : res = new String("\t\tpublic   \t");
                break;
            case 1 : res = new String("\t\tprivate  \t");
                break;
            case 2 : res = new String("\t\tprotected\t");
                break;
            default: System.err.println("INVALID VISIBILITY");
        }

        return res.concat(this.type).concat("\t").concat(this.name);
    }

    /**
     * Display the class
     */
    public void print() {
        // FIXME : print
        System.out.print(this.visibility);
        System.out.print("\t");
        System.out.print(this.type);
        System.out.print("\t");
        System.out.println(this.name);
    }

    private String getVisibilityString() {
        switch (this.visibility) {
            case 0 :
                return "public";
            case 1 :
                return "private";
            case 2 :
                return "protected";
            default:
                System.err.println("UNKNOWN VISIBILITY VALUE : ".concat(this.toString()));
                return null;
        }
    }

    public String getXML(String prefix, String indent) {
        String result = new String();

        result += prefix;
        result += "<attribute ";
        result = result.concat("visibility=\"").concat(this.getVisibilityString()).concat("\" ")
                .concat((null != type && !type.isEmpty()) ? "type=\"".concat(type).concat("\" ") : "")
                .concat((null != name && !name.isEmpty()) ? "name=\"".concat(name).concat("\" ") : "")
                .concat("/>\n");

        return result;
    }

    public String getXML() {
        return this.getXML("", "");
    }
}
