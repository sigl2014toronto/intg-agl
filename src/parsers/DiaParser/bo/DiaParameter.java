package parsers.DiaParser.bo;

/**
 * Created with IntelliJ IDEA.
 * User: boltz_j
 * Date: 12/10/13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
public class DiaParameter {
    private String name = null;
    private String type = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void print() {
        // FIXME : print
        //To change body of created methods use File | Settings | File Templates.
        System.out.print(this.getType().concat(" ").concat(this.name));
    }

    public String getDescription() {
        return this.getType().concat(" ").concat(this.getName());  //To change body of created methods use File | Settings | File Templates.
    }


    public String getXML(String prefix, String tab) {
        String result = new String();
        result += prefix;
        result += "<parameter";
        result += (null != type && !type.isEmpty()) ? " type=\"".concat(type).concat("\"") : "";
        result += (null != name && !name.isEmpty()) ? " name=\"".concat(name).concat("\"") : "";
        result += "/>\n";

        return result;
    }
    public String getXML() {
        return this.getXML("", "");
    }
}
