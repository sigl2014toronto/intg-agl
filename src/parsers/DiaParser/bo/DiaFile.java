package parsers.DiaParser.bo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Author:  boltz_j
 * Date:    20/10/13 @ 00:26
 */
public class DiaFile {
    private Map<Integer, DiaClass> classes;

    public DiaFile() {
        classes = new HashMap<>();
    }

    public void print() {
        for (DiaClass diaClass : classes.values()) {
            diaClass.print();
        }
    }

    public void addClass(DiaClass diaClass) {
        classes.put(diaClass.getId(), diaClass);
    }

    public DiaClass getClass(Integer id) {
        if (classes.containsKey(id))
            return classes.get(id);
        return null;
    }

    public String getDescription() {
        String result = new String("\n");

        for (DiaClass diaClass : classes.values()) {
            result = result.concat("<class name=\"")
                    .concat(diaClass.getName()).concat("\" ")
                    .concat(diaClass.getAbstract() ? "abstract=\"true\" " : "")
                    .concat(diaClass.getSuperClass() != null ? "superclass=\"".concat(diaClass.getSuperClass().getName()).concat("\"") : "")
                    .concat(">\n")
                    .concat(diaClass.getDescription())
                    .concat("</class>\n");
        }

        System.out.println(result);
        return result;
    }

    public String getXML() {
        return this.getXML("");
    }

    public String getXML(String indent) {
        // Open Tag
        String result = "<Design>\n";

        for (DiaClass diaClass : classes.values()) {
            result += diaClass.getXML(indent, indent);
        }

        // Close Tag
        result += "</Design>\n";

        return result;
    }
}
